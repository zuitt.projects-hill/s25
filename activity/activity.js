

db.fruits.aggregate([
		{
			$match: {"supplier" : "Red Farms Inc."}
		},
		{
			$count: "totalNoOfItems"
		}
	])

db.fruits.aggregate([
		{
			$match: {"price" : {$gt: 50}}
		},
		{
			$count: "priceGT50"
		}
	])

db.fruits.aggregate([
		{
			$match: {"onSale" : true}
		},
		{
			$group: {_id : "$supplier", averagePrice: {$avg: "$price"}}
		}
	])

db.fruits.aggregate([
		{
			$match: {"onSale" : true}
		},
		{
			$group: {_id : "$supplier", maxStocks: {$max: "$price"}}
		}
	])

db.fruits.aggregate([
		{
			$match: {"onSale" : true}
		},
		{
			$group: {_id : "$supplier", minStock: {$min: "$price"}}
		}
	])
